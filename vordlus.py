# Ülesanne:
# Lase kasutajal sisestada 2 arvu
# Programm peab võrdlema neid arve
# 1) võrdluse tulemus salvestatatakse järgmiselt:
#       0 kui arvud on võrdsed
#       1 kui esimene arv on suurem kui teine
#       -1 kui esimene arv on väiksem kui teine
#  2) võrdluse tulemuse järgi toimub väljastamine
# kas arvud on võrdsed või üks on suurem kui teine
# või üks on väiksem kui teine


# kasutaja sisend
arv1 = int(input('arv1 = '))
arv2 = int(input('arv2 = '))

#võrdleme
if(arv1 == arv2): tulemus = 0
elif(arv1 > arv2): tulemus = 1
else: tulemus = -1

#väljastus
if(tulemus == 0): print(str(arv1) + ' = ' + str(arv2))
if(tulemus == 1): print(str(arv1) + ' > ' + str(arv2))
if(tulemus == -1): print(str(arv1) + ' < ' + str(arv2))