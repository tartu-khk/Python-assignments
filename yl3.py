# Koosta programm, mis
# arvutab arvude 1 kuni 10 summat
# ja trykib summa ekraanile
# kasutada tuleb for tsyklit

summa = 0
for arv in range(1, 11, 1):
    summa = summa + arv  # lühendatud variant    summa += arv
    print('arv = ' + str(arv))
    print('hetkel summa =' + str(summa))
    print('---------------')

# kirjuta programm nii,ymber,
# et lahendus oleks koostatud
# while tsykli kasutamisega

summa = 0
arv = 1
while(arv < 11):
    summa = summa + arv  # lühendatud variant    summa += arv
    arv += 1
    print('arv = ' + str(arv))
    print('hetkel summa =' + str(summa))
    print('---------------')

