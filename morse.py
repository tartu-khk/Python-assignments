tahestik = {"a": ".-",
            "b": "-...",
            "c": "-.-.",
            "d": "-..",
            "e": ".",
            "f": "..-.",
            "g": "--.",
            "h": "....",
            "i": "..",
            "j": ".---",
            "k": "-.-",
            "l": ".-..",
            "m": "--",
            "n": "-.",
            "o": "---",
            "p": ".--.",
            "q": "--.-",
            "r": ".-.",
            "s": "...",
            "t": "-",
            "u": "..-",
            "v": "...-",
            "w": ".--",
            "x": "-..-",
            "y": "-.--",
            "z":"--..",
            ' ': '_',
            '?': '..--..'}

def convert(lause):
    lause = lause.lower()
    encodedLause = ""
    for character in lause:
        encodedLause += tahestik[character] + " "
    return encodedLause

print("Tere, mina olem morse sõnastik!")
lause = "Tere mina olem morse sonastik"
encodedLause = convert(lause)
print(encodedLause)

lause = input("Sisesta oma lause: ")
encodedLause = convert(lause)
print(encodedLause)