# Ülesanne:
# koosta programm, mis küsib kasutaja käest ruutvõrrandi numbrilised väärtused
# a, b ja c
# ja väljastada, mitu lahendit antud juhul ruutvõrrandil on

import math
a = float(input('a = '))
b = float(input('b = '))
c = float(input('c = '))

# kas üldse on ruutvõrrand?
if(a == 0):
    #ei ole ruutvõrrand
    print('See ei ole ruutvõrrand')
else:
    # on ruutvõrrand
    # d arvutamine
    d = math.pow(b, 2) - 4 * a * c
    print(d)
    # kontrollime lahendite olemasolu
    if(d > 0):
        print('Võrrandil on 2 lahendit')
    elif(d == 0):
        print('Võrrandil on 1 lahend')
    else:
        # d < 0
        print('Lahendid puuduvad')