# Ülesanne:
# Luba kasutajale sisestada kuu numbri kujul
# Vastavalt sellele programm väljastab aastaaja


# kasutaja sisend
kuu = int(input('Sisesta kuu (1-12): '))

# 3 4 5 kevad
if(kuu > 2 and kuu < 6):
    teade = 'kevad'

# 6 7 8 suvi
elif(kuu > 5 and kuu < 9):
    teade = 'suvi'

# 9 10 11 sügis
elif(kuu > 8 and kuu < 12):
    teade = 'sügis'

# 12 1 2 talv
elif(kuu == 1 or kuu == 2 or kuu == 12):
    teade = 'talv'

if(kuu > 0 and kuu < 13):
    print('Antud kuu on ' + teade)

else:
    print('Meie aastas ei ole sellise numbriga kuud')