# Koosta programm, mis
# arvutab arvude 5 faktoriaal
# ja trykib faktoriaali väärtuse
# kasutada tuleb for tsyklit
# 5! = 1 * 2 * 3 * 4 * 5

fakt = 1
for arv in range(1, 6, 1):
    fakt = fakt * arv
    print('5! = ' + str(fakt))