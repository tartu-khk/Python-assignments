# Ülesanne:
# luba kasutajale sisestada tema nimi
# luba kasutajale sisestada vanus
# aga pöördu tema poole viisakalt
# määra vanus 18 kuni 40 (k.a) kuuluvusse teatud klubisse
# muidu teavita, et klubi sisse pääseda ei saa


# kasutaja sisendid
nimi = input('Kuidas on sinu nimi? ')
vanus = int(input(nimi + ', kui vana sa oled? '))

# kontrollime, kas vanus on sobivas vahemikus
if(vanus > 17 and vanus < 41):
    print(nimi + ', tere tulemast 18-40 klubisse!')
else:
    print('Vabandust ' + nimi + ', 18-40 klubisse sa ei pääse.')